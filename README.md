## go-xbulk

## What?

The go-xbulk utility builds a set of xbps packages with xbps-src,
by running `xbps-src show-build-deps <pkg>` to build a dependency
tree on the fly.

As the dependency tree is being built, terminal dependencies are built
and packaged on the fly.

As these builds complete additional dependencies may be satisfied and be
added to the build order. Ultimately the entire tree is built.

Only one attempt is made to build any given package, no matter how many
other packages depend on it.

go-xbulk is specifically designed for xbps and xbps-src (could be adapted
to other package systems with a few tweaks).

## Requirements

Before running go-xbulk, masterdir must be fully populated (`xbps-src binary-bootstrap`)
and `XBPS_CHROOT_CMD` must be set to uchroot, i.e:

```
$ echo "XBPS_CHROOT_CMD=uchroot" >> void-packages/etc/conf
```

A small patch needs to be applied to `void-packages` for proper behaviour:

```
--- a/common/xbps-src/shutils/show.sh
+++ b/common/xbps-src/shutils/show.sh
@@ -80,6 +80,8 @@ show_pkg_build_depends() {
     local f x _pkgname _srcpkg found result
     local _deps="$1"

+    check_pkg_arch "$XBPS_CROSS_BUILD"
+
     result=$(mktemp) || exit 1

     # build time deps
```

## Details

go-xbulk has three operation modes:

 * System mode (`-s`): only builds packages that were installed manually in your system.
   This list can be obtained with `xbps-query -m`.
 * Package mode (`-p`): only builds the specified packages declared in command line.
 * World mode (`-w`): builds all packages available at `void-packages/srcpkgs`.

System mode and package mode can be used together, and world mode cannot be used with
the other modes because this mode builds everything!

The flags that are passed in to xbps-src when building packages are `-E`, `-N`, and `-t`:

    -E checks binary package exists in local repository to avoid unnecessary rebuilds
    -N does not use remote repositories
    -t uses xbps-uchroot with overlayfs

The following commands are part of my workflow to have my systems up-to-date:
```
$ cd void-packages && git pull
$ ./xbps-src -N clean <- this ensures that no remote repos are declared in masterdir/etc/xbps.d
$ go-xbulk -s
$ xbps-rindex -r /path/to/hostdir/binpkgs
# xbps-install -yu
# xbps-remove -Oo
```

## The end
Note that some packages require lots of memory to build, for example firefox needs
at least 16GB, and this is without debugging packages (`XBPS_DEBUG_PKGS`).

Have fun and happy building! golang is really cool and easy to learn if you
already know C or C++ :-)

```
$ ./go-xbulk -h
Usage: ./go-xbulk [options]
  -d string
    	Path to void-packages repository (default "/home/juan/projects/void-linux/void-packages")
  -j int
    	Number of parallel builds (default 4)
  -p string
    	Build specified packages. Multiple packages can be declared like "-p "foo bar""
  -s	System mode, builds packages that were manually installed in your system (xbps-query -m)
  -w	World mode, builds all packages available at void-packages/srcpkgs

Copyright (c) 2020 Juan Romero Pardines <xtraeme@gmail.com>
$
```

