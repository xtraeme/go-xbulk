/*
 Copyright (c) 2020 Juan Romero Pardines. All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package main

import (
	"flag"
	"fmt"
	"github.com/go-cmd/cmd"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

type Package struct {
	name    string
	exit    int
	rdcount int
	state   int
	rdeps   []*Package
	RunCmd  *cmd.Cmd
}

const (
	WAITING = iota
	SKIPPED
	DEPFAIL
	BUILD
	RUN
	DONE
)

var (
	NParallel int
	NRunning  int
	LogDir    string
	ItemList  []*Package
	BuildList []*Package
	RunList   []*Package
)

func WriteLog(file string, data []byte) {
	f, err := os.OpenFile(file, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	ErrCheck(err)
	_, err = f.Write(data)
	ErrCheck(err)
	f.Close()
}

func ErrCheck(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

func LookupItem(name string) *Package {
	for _, pkg := range ItemList {
		if pkg.name == name {
			return pkg
		}
	}
	return nil
}

func AddItem(Name string) *Package {
	pkg := &Package{name: Name}
	ItemList = append(ItemList, pkg)
	return pkg
}

func AddDepn(pkg *Package, rdep *Package) {
	rdep.rdeps = append(rdep.rdeps, pkg)
	if rdep.state == DONE {
		status := rdep.RunCmd.Status()
		if status.Exit != 0 {
			// reverse dependency build failed, so that pkg also failed!
			pkg.state = DEPFAIL
			pkg.exit = status.Exit
			log := fmt.Sprintf("%s/deps/%s.txt", LogDir, pkg.name)
			val := rdep.name + "\n"
			WriteLog(log, []byte(val))
		}
	} else {
		pkg.rdcount++
	}
}

func AddBuild(pkg *Package) {
	pkg.state = BUILD
	BuildList = append(BuildList, pkg)
	fmt.Println("BuildOrder", pkg.name)
}

func ProcessCompletion(pkg *Package) {
	if pkg.state == RUN || pkg.state == SKIPPED {
		var log string
		if pkg.exit == 0 {
			log = "good"
		} else if pkg.exit == 2 {
			log = "skipped"
		} else {
			log = "bad"
		}
		log = fmt.Sprintf("%s/%s/%s.txt", LogDir, log, pkg.name)
		status := pkg.RunCmd.Status()
		data := make([]string, 0)
		data = append(data, status.Stdout...)
		data = append(data, status.Stderr...)
		f, err := os.OpenFile(log, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		ErrCheck(err)
		for _, s := range data {
			_, err := f.WriteString(s + "\n")
			ErrCheck(err)
		}
		f.Close()
	}
	pkg.state = DONE
	fmt.Printf("Finished %s (exitcode: %d)\n", pkg.name, pkg.exit)
	for _, rdep := range pkg.rdeps {
		if rdep.rdcount > 0 {
			rdep.rdcount--
		}
		if rdep.state == WAITING || rdep.state == DEPFAIL {
			if pkg.exit == 0 {
				if rdep.rdcount == 0 {
					if rdep.state == WAITING {
						AddBuild(rdep)
					} else {
						ProcessCompletion(rdep)
					}
				}
			} else {
				rdep.exit = pkg.exit
				rdep.state = DEPFAIL
				log := fmt.Sprintf("%s/deps/%s.txt", LogDir, rdep.name)
				val := pkg.name + "\n"
				WriteLog(log, []byte(val))
				ProcessCompletion(rdep)
			}
		}
	}
}

func WaitRunning() bool {
	if len(RunList) == 0 {
		return false
	}
	for i := 0; i < len(RunList); i++ {
		pkg := RunList[i]
		status := pkg.RunCmd.Status()
		if status.Complete {
			// process terminated
			pkg.exit = status.Exit
			ProcessCompletion(pkg)
			if NRunning > 0 {
				NRunning--
			}
			// remove from RunList
			RunList = append(RunList[:i], RunList[i+1:]...)
			i--
		}
	}
	// 100ms is enough! CPU usage reduced by 70%
	time.Sleep(100 * time.Millisecond)
	return true
}

// Trigger builds by maintaining NParallel jobs.
func RunBuilds(vpkgsdir string) {
	for NRunning < NParallel {
		if len(BuildList) == 0 {
			return
		}
		pkg := BuildList[0]
		BuildList = BuildList[1:]
		xbpssrc := vpkgsdir + "/xbps-src"
		pkg.RunCmd = cmd.NewCmd(xbpssrc, "-ENt", "pkg", pkg.name)
		pkg.state = RUN
		NRunning++
		RunList = append(RunList, pkg)
		// non-blocking
		_ = pkg.RunCmd.Start()
		fmt.Println("Building", pkg.name)
	}
}

// Recursively finds build dependencies by running "./xbps-src show-build-deps <pkg>".
func CollectDeps(vpkgsdir string, Name string) *Package {
	fmt.Println("Checking", Name)
	pkg := AddItem(Name)
	xbpssrc := vpkgsdir + "/xbps-src"
	xbpssrcCmd := cmd.NewCmd(xbpssrc, "show-build-deps", Name)
	// blocking
	statusCmd := <-xbpssrcCmd.Start()
	if statusCmd.Exit != 0 {
		// xbps-src returns 1 on error and 2 if pkg is broken or unsupported arch
		pkg.state = SKIPPED
		pkg.RunCmd = xbpssrcCmd
		pkg.exit = statusCmd.Exit
		pkg.rdcount = 0
	}
	for _, depname := range statusCmd.Stdout {
		if depname == "" {
			continue
		}
		dep := LookupItem(depname)
		if dep == nil {
			dep = CollectDeps(vpkgsdir, depname)
		}
		AddDepn(pkg, dep)
	}
	if pkg.rdcount == 0 {
		// If pkg has no dependencies left either add it to the
		// build list or do completion processing (i.e. if some
		// of the dependencies failed or cannot be built).
		switch pkg.state {
		case WAITING:
			AddBuild(pkg)
		case SKIPPED, DEPFAIL:
			ProcessCompletion(pkg)
		default:
			fmt.Printf("%s state %d, error!\n", pkg.name, pkg.state)
			os.Exit(1)
		}
	} else {
		fmt.Println("Deferred", pkg.name, "rdcount", pkg.rdcount)
	}
	RunBuilds(vpkgsdir)
	return pkg
}

func GetBasePkg(srcpkgs string, pkgname string) string {
	// if symlink, convert to $sourcepkg
	path := srcpkgs + "/" + pkgname
	bpkg, err := filepath.EvalSymlinks(path)
	ErrCheck(err)
	return filepath.Base(bpkg)
}

// Find out list of manually installed pkgs
func GetSystemPkgs(srcpkgs string) []string {
	out, err := exec.Command("xbps-query", "-m").Output()
	ErrCheck(err)

	pkgs := make([]string, 0)
	for _, v := range strings.Split(string(out), "\n") {
		if v == "" {
			continue
		}
		// get pkgname prefix
		idx := strings.LastIndex(v, "-")
		if idx != -1 {
			v = v[:idx]
		}
		bpkg := GetBasePkg(srcpkgs, v)
		var found bool
		for _, x := range pkgs {
			if x == bpkg {
				found = true
				break
			}
		}
		if found == false {
			pkgs = append(pkgs, bpkg)
		}
	}
	return pkgs
}

func GetAllPkgs(srcpkgs string) []string {
	var list []string

	files, err := ioutil.ReadDir(srcpkgs)
	ErrCheck(err)

	for _, file := range files {
		bpkg := GetBasePkg(srcpkgs, file.Name())
		// do not add dups to the final slice
		var found bool
		for _, x := range list {
			if x == bpkg {
				found = true
				break
			}
		}
		if found == false {
			list = append(list, bpkg)
		}
	}
	return list
}

func main() {
	var pkglist []string
	var vpkgsdir string
	var pkgmode string
	var worldmode bool
	var sysmode bool

	Homedir, err := os.UserHomeDir()
	ErrCheck(err)
	vdir := Homedir + "/projects/void-linux/void-packages"
	t := time.Now().Local()
	year, month, day := t.Date()
	hours, minutes, secs := t.Clock()
	tstamp := fmt.Sprintf("%d%d%d%d%d%d", year, month, day, hours, minutes, secs)
	LogDir = "xbulk-" + tstamp

	flag.IntVar(&NParallel, "j", runtime.NumCPU(), "Number of parallel builds")
	flag.StringVar(&vpkgsdir, "d", vdir, "Path to void-packages repository")
	flag.StringVar(&pkgmode, "p", "", "Build specified packages. Multiple packages can be declared like -p \"foo bar\"")
	flag.BoolVar(&sysmode, "s", false, "System mode, builds packages that were manually installed in your system (xbps-query -m)")
	flag.BoolVar(&worldmode, "w", false, "World mode, builds all packages available at void-packages/srcpkgs")
	flag.Usage = func() {
		fmt.Printf("Usage: %s [options]\n", os.Args[0])
		flag.PrintDefaults()
		fmt.Printf("\nCopyright (c) 2020 Juan Romero Pardines <xtraeme@gmail.com>\n")
	}
	flag.Parse()

	srcpkgs := vpkgsdir + "/srcpkgs"
	ldirs := []string{"good", "bad", "deps", "skipped"}
	for _, x := range ldirs {
		dir := LogDir + "/" + x
		err := os.MkdirAll(dir, 0755)
		ErrCheck(err)
	}

	if !sysmode && !worldmode && pkgmode == "" {
		fmt.Printf("%s: -p, -s or -w wasn't set, at least one of them needs to be set. Exiting...\n", os.Args[0])
		os.Exit(1)
	}
	if worldmode && (sysmode || pkgmode != "") {
		fmt.Printf("%s: -p and -s cannot be used with -w. Exiting...\n", os.Args[0])
		os.Exit(1)
	}

	if worldmode {
		pkglist = append(pkglist, GetAllPkgs(srcpkgs)...)
	} else {
		if sysmode {
			pkglist = append(pkglist, GetSystemPkgs(srcpkgs)...)
		}
		if pkgmode != "" {
			pkglist = append(pkglist, strings.Split(pkgmode, " ")...)
		}
	}

	for _, name := range pkglist {
		dep := LookupItem(name)
		if dep == nil {
			_ = CollectDeps(vpkgsdir, name)
		}
	}

	RunBuilds(vpkgsdir)
	for WaitRunning() {
		RunBuilds(vpkgsdir)
	}
}
